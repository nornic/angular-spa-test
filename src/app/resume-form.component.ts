import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Resume} from './resume-model/resume.model';

@Component({
  selector: 'app-root',
  templateUrl: './resume-form.component.html',
})
export class ResumeFormComponent implements OnInit {
    private step = 1;
    private response: { is_success: boolean; content: string[]};

    resumeModel = new Resume();
    resumeForm: FormGroup;

    constructor(private http: HttpClient) {}
    ngOnInit() {
        this.resumeForm = new FormGroup({
            'initials': new FormControl(null, [
                Validators.required,
                Validators.pattern(/^[А-я\-]{2,}\s+([А-я\-](\s+|))+$/)
            ]),
            'gender': new FormControl(null , [Validators.required]),
            'dependent_count': new FormControl(null, [
                Validators.pattern(/^[0-9]+$/)
            ]),
            'partner_state': new FormControl(null),
            'birthdate': new FormControl(null, []),
            'email': new FormControl(null, [
                Validators.required,
                Validators.email,
            ]),

        });
    }

    onSubmit() {
        Object.keys(this.resumeForm.controls).forEach(field => {
            const control = this.resumeForm.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }
        });

        if (!this.resumeForm.valid) {
            return;
        }

        let self = this;
        const body = this.resumeForm.value;
        this.http.post('//192.168.34.88/check', body)
            .subscribe(function(response) {
                if (response['is_success'] && response['content']) {
                    self.step = 2;
                    self.response = {
                        is_success: response['is_success'],
                        content: response['content']
                    };

                    self.resumeModel.load(self.response.content);
                } else if(response['error']) {
                    for (let field in response['error']) {
                        self.resumeForm.get(field).setErrors({'server': response['error'][field]});
                    }
                } else {
                    alert('API error');
                }
            });
    }

    showStepOne() {
        return this.step === 1;
    }

    showStepTwo() {
        return this.step === 2;
    }
}
