import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Resume} from '../../resume-model/resume.model';

@Component({
  selector: 'app-gender-editor',
  templateUrl: './gender-editor.component.html',
})

export class GenderEditorComponent {
    @Input() resumeForm: FormGroup;
    @Input() resumeModel: Resume;

}


