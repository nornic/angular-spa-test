import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-birthdate-editor',
  templateUrl: './birthdate-editor.component.html'
})
export class BirthdateEditorComponent {
    @Input() resumeForm: FormGroup;
}
