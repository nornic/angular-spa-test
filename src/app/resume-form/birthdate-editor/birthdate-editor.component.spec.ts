import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BirthdateEditorComponent } from './birthdate-editor.component';

describe('BirthdateEditorComponent', () => {
  let component: BirthdateEditorComponent;
  let fixture: ComponentFixture<BirthdateEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BirthdateEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirthdateEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
