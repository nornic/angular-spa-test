import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
    selector: 'app-initials-editor',
    templateUrl: './initials-editor.component.html',
})

export class InitialsEditorComponent {
    @Input() resumeForm: FormGroup;
}


