import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitialsEditorComponent } from './initials-editor.component';

describe('InitialsEditorComponent', () => {
  let component: InitialsEditorComponent;
  let fixture: ComponentFixture<InitialsEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitialsEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitialsEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
