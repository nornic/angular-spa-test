import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-email-editor',
  templateUrl: './email-editor.component.html'
})
export class EmailEditorComponent {
    @Input() resumeForm: FormGroup;
}
