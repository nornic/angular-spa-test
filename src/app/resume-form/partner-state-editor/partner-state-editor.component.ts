import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Resume} from '../../resume-model/resume.model';

@Component({
  selector: 'app-partner-state-editor',
  templateUrl: './partner-state-editor.component.html',
})
export class PartnerStateEditorComponent {
  @Input() resumeForm: FormGroup;
  @Input() resumeModel: Resume;

  couldHavePartner() {
      if (this.resumeForm.get('birthdate').value === null) {
        return false;
      }
      const birthdate = new Date(this.resumeForm.get('birthdate').value);
      const when_eighteen = birthdate.setFullYear(birthdate.getFullYear() + 18);
      const now = (new Date()).getTime();

      return (when_eighteen - now) <= 0;
  }
}

