import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerStateEditorComponent } from './partner-state-editor.component';

describe('IsSingleEditorComponent', () => {
  let component: PartnerStateEditorComponent;
  let fixture: ComponentFixture<PartnerStateEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerStateEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerStateEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
