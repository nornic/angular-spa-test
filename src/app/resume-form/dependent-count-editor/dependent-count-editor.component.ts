import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-dependent-count-editor',
  templateUrl: './dependent-count-editor.component.html'
})
export class DependentCountEditorComponent {
    @Input() resumeForm: FormGroup;

    couldHaveDependent() {
        return (this.resumeForm.get('partner_state').value);
    }

}
