import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DependentCountEditorComponent } from './dependent-count-editor.component';

describe('DependentCountEditorComponent', () => {
  let component: DependentCountEditorComponent;
  let fixture: ComponentFixture<DependentCountEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DependentCountEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DependentCountEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
