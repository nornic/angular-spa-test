import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ResumeFormComponent } from './resume-form.component';

import { InitialsEditorComponent } from './resume-form/initials-editor/initials-editor.component';
import { GenderEditorComponent } from './resume-form/gender-editor/gender-editor.component';
import { BirthdateEditorComponent } from './resume-form/birthdate-editor/birthdate-editor.component';
import { PartnerStateEditorComponent } from './resume-form/partner-state-editor/partner-state-editor.component';
import { DependentCountEditorComponent } from './resume-form/dependent-count-editor/dependent-count-editor.component';
import { EmailEditorComponent } from './resume-form/email-editor/email-editor.component';

import { MatRadioModule, MatCheckboxModule} from '@angular/material';
import { MatNativeDateModule, MatDatepickerModule } from '@angular/material';

import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    ResumeFormComponent,
    InitialsEditorComponent,
    GenderEditorComponent,
    BirthdateEditorComponent,
    PartnerStateEditorComponent,
    DependentCountEditorComponent,
    EmailEditorComponent,
  ],
  imports: [
      HttpClientModule, BrowserModule, MatNativeDateModule, ReactiveFormsModule, MatDatepickerModule, MatRadioModule, MatCheckboxModule, BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [ResumeFormComponent],
})

export class AppModule { }
