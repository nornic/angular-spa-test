export class Resume {
    private initials: string = null;
    private gender: string = null;
    private birthdate: string = null;
    private partner_state: string = null;
    private dependent_count: number = null;
    private email: string = null;

    public gender_list: Object[] = [
        {id:  1, value: 'Мужской'},
        {id:  2, value: 'Женский'}
        ];
    private boolean_true_state = 'Да';

    private field_list = new Object({
        initials: 'Инициалы',
        gender: 'Пол',
        birthdate: 'Дата рождения',
        partner_state: 'Женат/замужем',
        dependent_count: 'Количество детей',
        email: 'Электронная почта'
    });

    getFieldValueList() {
        let result: string[] = [];
        for (let field in this.field_list) {
            if (this[field]) {
                const field_name = this.field_list[field];
                let field_value = this[field];
                if (this[field] === true) {
                   field_value = this.boolean_true_state;
                }
                result.push(field_name + ': ' + field_value);
            }
        }
        return result;
    }

    load(model): void {
        this.initials = null;
        this.gender = null;
        this.birthdate = null;
        this.partner_state = null;
        this.email = null;
        this.dependent_count = null;

        for (let key in model) {
            if (this.hasOwnProperty(key)) {
                this[key] = model[key];
            }
        }
    }
}
